/**
 * 
 */
package com.ruanko.view;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.jdom.JDOMException;

import com.ruanko.model.Channel;
import com.ruanko.model.MyToolkits;
import com.ruanko.model.News;
import com.ruanko.service.Browser;
import com.ruanko.service.Download;
import com.ruanko.service.RSSService;
import com.ruanko.service.ViewSetting;

import java.util.Date;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
/**
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class JMainFrame extends JFrame
{
	public final static int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width*3/5;			// 设置界面宽度为计算机屏幕宽度的2/3；
	public final static int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height*3/4;		// 设置界面高度为计算机屏幕高度的3/4 

	// 菜单栏大小设置
	// 工具栏大小设置
	// 表格区大小设置
	private final static int tableHeight = HEIGHT*2/5;		// 设置表格高度
	private final static int tableWidth = WIDTH - 50;			// 设置表格宽度
	// 信息显示区大小设置
	private final static int jlWidth = 50;					// 标签宽度
	private final static int jtHeight = 25;					// 文本框高度，也是标签高度
	private final static int jtTitleWidth = WIDTH - 100;	// 标题宽度
	private final static int jtLinkWidth = WIDTH - 200;		// 链接宽度
	private final static int jtAuthorWidth = WIDTH/2 - 80;	// 作者宽度
	private final static int jtPubDateWidth = WIDTH/2 - 80;	// 发布日期宽度
	// 新闻显示区大小设置
	private final static int jtNewsHeight = HEIGHT/5;		// 新闻显示区的高度
	private final static int jtNewsWidth = WIDTH - 50;			// 新闻显示区的宽度

	private final static String TITLE = "RSS阅读器";			// 基本界面标题 

	// 控件成员
	private JMenuBar jmBar;						// 菜单栏
	private JToolBar jtBar;						// 工具栏
	private JTable jtTable;						// 表格
	private JFileChooser jfChooser;				// 文件保存对话框
	public static JLabel jlState;				// 状态栏显示标签

	private JTextField jtTitle;						// 用以显示新闻详细信息：标题
	private JTextField jtLink;						// 用以显示新闻详细信息：链接
	private JTextField jtAuthor;					// 用以显示新闻详细信息：作者
	private JTextField jtPubDate;					// 用以显示新闻详细信息：发布日期
	private JTextArea jtNews;						// 用以显示新闻详细信息：简介
	// 服务对象成员
	private RSSService rssService;					// RSS服务对象
	private DefaultTableModel dtmTableModel;		// 表格数据模型
	private List<News> newsList;					// 新闻列表

	// 默认构造方法
	public JMainFrame()
	{
		// 创建服务对象
		rssService = new RSSService();
		// 基本界面设置 
		setTitle(TITLE);
		setSize(WIDTH, HEIGHT);
		setIconImage(new ImageIcon("./images/rss.png").getImage());
		setResizable(false);
		// 居中显示
		setCenter();
		setLayout(new BorderLayout());
		// 设置主面板
		add(getJPMain(), BorderLayout.CENTER);
		// 设置菜单栏
		setJMenuBar(getJMbar());		
		// 设置状态栏
		add(getStateBar(), BorderLayout.SOUTH);
		// 设置界面关闭事件
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				setVisible(false);
				// 释放资源
				dispose();
				System.out.println("正在退出RSS阅读器...");
				// 保存文件
				rssService.saveChannel();
				// 正式退出
				System.exit(0);
			}
		});
	}
	// 获取主面板
	private JPanel getJPMain()
	{
		// 创建父组件
		JPanel jpMain = new JPanel(new BorderLayout());
		// 向主面板中添加组件
		jpMain.add(getJTbar(), BorderLayout.NORTH);					// 添加工具栏
		jpMain.add(getJSPTable(), BorderLayout.CENTER);				// 添加表格
		jpMain.add(getJPNewsDescription(), BorderLayout.SOUTH);		// 添加新闻显示区
		// 返回父组件
		return jpMain;
	}
	// 获取工具栏
	private JToolBar getJTbar()
	{
		jtBar = new JToolBar();
		// 设置导出图标和按钮
		ImageIcon icon = new ImageIcon("./images/export.png");
		JButton jbExport = new JButton(icon);
		// 设置导入图标和按钮
		ImageIcon icon2 = new ImageIcon("./images/import.png");
		JButton jbImport = new JButton(icon2);
		// 设置导出提示
		jbExport.setToolTipText("将新闻导出");
		// 设置导入提示
		jbImport.setToolTipText("将新闻导入");
		// 添加导出事件处理
		jbExport.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 设置文件选择框初始化目录
				jfChooser = new JFileChooser("./NewsFiles");
				// 设置文件选择框标题
				jfChooser.setDialogTitle("导出新闻文件");
			//  设置文件过滤器
			//	FileNameExtensionFilter filter = new FileNameExtensionFilter("txt files", "txt", "docx");
			//	jfChooser.setFileFilter(filter);

				// 设置默认保存的文件名
				File exportNews = new File("NewsFiles/News.txt");
				if (!exportNews.exists())
					try {
						exportNews.createNewFile();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
				jfChooser.setSelectedFile(exportNews);
				// 获取用户选择
				News news = newsList.get(jtTable.getSelectedRow());
				// 调用文件保存对话框
				int result = jfChooser.showSaveDialog(null);
				// 获取用户按键输入
				if (result == JFileChooser.APPROVE_OPTION)
				{
					if (rssService.saveNews(news, jfChooser.getSelectedFile()))
						JOptionPane.showMessageDialog(null, "新闻信息保存成功");
					else
						JOptionPane.showMessageDialog(null, "新闻信息保存失败");
				}
			}
		});
		// 添加导入事件处理
		jbImport.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 设置文件选择框初始化目录
				jfChooser = new JFileChooser("./NewsFiles");
				// 设置文件选择框标题
				jfChooser.setDialogTitle("导入新闻文件");
			//	设置文件过滤器
			//	FileNameExtensionFilter filter = new FileNameExtensionFilter("txt files", "txt", "docx");
			//	jfChooser.setFileFilter(filter);

				// 获取用户选择
				int result = jfChooser.showOpenDialog(null);

				if (result == JFileChooser.APPROVE_OPTION)
				{
					// 获取用户选择的文件
					File file = jfChooser.getSelectedFile();
					// 导入成功则将其显示到表格中
					try {
						// 加载XML文件失败
						if (rssService.load(file) == null)
						{
							JOptionPane.showMessageDialog(null, "本地加载频道失败！");
							return ;
						}
						else
						{
							// 如果存在该新闻列表，则显示出来
							newsList = rssService.getNewsList(file);
							showTable(newsList);
						}
						// 设置状态栏
						Date updateDate = new Date(file.lastModified());
						String str = String.format("%tF %tR",updateDate, updateDate);
						jlState.setText("(更新到"+str+")  "+(new Date()));
						showTable(newsList);
					} catch (JDOMException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// 针对每次读取新的内容，都动态的调整表格的宽度
					jtTable = MyToolkits.autoTableWidth(jtTable, dtmTableModel);
				}
			}
		});
		// 将文件导出按钮添加到工具栏
		jtBar.add(jbExport);
		// 将文件导入按钮添加到工具栏
		jtBar.add(jbImport);
		return jtBar;
	}
	// 获取状态栏
	private JPanel getStateBar()
	{
		JPanel main = new JPanel(new BorderLayout(10, 10));
		// 设置状态显示
		jlState = new JLabel("");
		// 设置时间显示
		JLabel jlStateDate = new JLabel("\t"+String.format("%tF %tR",new Date(), new Date()));
		// 将两个标签都添加到状态栏
		main.add(jlState, BorderLayout.WEST);
		main.add(jlStateDate, BorderLayout.EAST);

		return main;
	}

	// 获取菜单栏
	private JMenuBar getJMbar()
	{
		// 设置父菜单
		jmBar = new JMenuBar();
		// 设置子菜单

		// 添加子菜单
		jmBar.add(getOneMenu());
		jmBar.add(getTwoMenu());
		jmBar.add(getThreeMenu());
		// 返回父菜单
		return jmBar;
	}
	// 获取第一个菜单
	/*
	 * 菜单名：文件
	 */
	private JMenu getOneMenu()
	{
		// 返回菜单
		return getOneFirstMenu();		
	}
	// 获取第一个菜单的一级菜单
	/*
	 * 父级菜单名：文件
	 * 子级菜单名一：打开
	 * 子级菜单名二：订阅RSS
	 * 子级菜单名三：退出
	 */
	private JMenu getOneFirstMenu()
	{
		// 创建父菜单
		JMenu menu = new JMenu("文件");
		// 设置子菜单
		getOneSecondMenu().setText("打开");
		JMenuItem menu2 = new JMenuItem("订阅RSS");
		JMenuItem menu3 = new JMenuItem("退出");
		// 子菜单事件处理
		// 处理订阅事件
		menu2.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 返回对话框让用户输入数据并订阅RSS    
				new CreateChannelFrame().setVisible(true);
			}
		});
		// 处理退出事件
		menu3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
				System.out.println("正在退出RSS阅读器...");
				// 保存文件
				rssService.saveChannel();
				// 正式退出
				System.exit(0);
			}
		});
		// 添加子菜单
		menu.add(getOneSecondMenu());
		menu.add(menu2);
		menu.add(menu3);
		// 返回父菜单
		return menu;
	}
	// 获取第一个菜单的二级菜单
	/*
	 * 父级菜单名：打开
	 * 子级菜单名一：本地RSS
	 * 子级菜单名二：网络串流
	 */
	private JMenu getOneSecondMenu()
	{
		// 创建父菜单
		JMenu menu = new JMenu("打开");
		// 设置子菜单
		JMenuItem item = new JMenuItem("网络串流");
		// 子菜单事件处理
		// 为网络串流添加事件处理
		item.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 弹出 对话框获取用户输入的URL地址并下载该文件
				final String url = JOptionPane.showInputDialog("请输入网络串流地址");
				// 如果取消输入，则返回
				if (url == null)
					return;
			//	设置进度条
			//	new MyProgressBar("正在为你从:"+url+"下载文件", 1);

			//	Download.resetValue();
				// 获取文件
				File file = Download.download(url.trim());
				// 获取文件出错
				if (file == null)
					return;
				
				// 获取新闻列表
				try {
					newsList = rssService.getNewsList(file);
					// 设置状态栏
					Date updateDate = new Date(file.lastModified());
					String str = String.format("%tF %tR",updateDate, updateDate);
					ViewSetting.setStateBar("(更新到"+str+")");
					// 显示数据
					showTable(newsList);
				} catch (JDOMException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// 针对每次读取新的内容，都动态的调整表格的宽度
				jtTable = MyToolkits.autoTableWidth(jtTable, dtmTableModel);
			}
		});
		// 添加子菜单
		menu.add(getOneThirdMenu());
		menu.add(item);
		// 返回父菜单
		return menu;
	}
	// 获取第一个菜单的三级菜单
	/*
	 * 父级菜单：本地RSS
	 * 子级菜单一到n ：各频道
	 */
	private JMenu getOneThirdMenu()
	{
		// 创建父菜单
		final JMenu result = new JMenu("本地RSS");
		// 添加事件处理
		// 父级本地RSS事件处理：当鼠标移动到这里时，添加频道菜单项
		result.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseEntered(MouseEvent e)
			{
				// 事先清空result原有的菜单项
				result.removeAll();
				// 根据频道列表创建并添加菜单项
				for(Channel channel : rssService.getChannelList())
				{
					JMenuItem item = new JMenuItem(channel.getName());
					// 添加事件处理
					item.addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent e)
						{
							for (Channel channel : rssService.getChannelList())
							{
								// 获取到用户点击的频道名称
								if (e.getActionCommand().equals(channel.getName()))
								{
									// 设置状态栏
									File file = new File(channel.getFilePath());
									Date updateDate = new Date(file.lastModified());
									ViewSetting.setStateBar(channel.getName()+"   "+"(更新到"+updateDate+")");
									// 获取到文件路径后加载该XML文件
									try {
										// 加载XML文件失败
										if (rssService.load(file) == null)
										{
											JOptionPane.showMessageDialog(null, "本地加载"+channel.getName()+"频道文件失败！");
											return;
										}
										else
										{
											// 如果存在该新闻列表，则显示出来
											newsList = rssService.getNewsList(file);
											showTable(newsList);
										}
									} catch (JDOMException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} catch (IOException e2) {
										// TODO Auto-generated catch block
										e2.printStackTrace();
									}
								}
							}				
							// 针对每次读取新的内容，都动态的调整表格的宽度
							jtTable = MyToolkits.autoTableWidth(jtTable, dtmTableModel);
						}
					});
					// 将该菜单项添加到result
					result.add(item);
				}
			}
		});
		// 返回父菜单
		return result;
	}

	// 获取第二个菜单
	/*
	 * 菜单：编辑
	 */
	private JMenu getTwoMenu()
	{
		return getTwoFirstMenu();
	}
	// 获取第二个菜单的一级菜单
	/*
	 * 父级菜单：编辑
	 * 子级菜单：编辑RSS源
	 * 子级菜单：更换样式
	 */
	private JMenu getTwoFirstMenu()
	{
		// 创建父菜单
		JMenu menu = new JMenu("编辑");
		// 设置子菜单
		JMenuItem edit = new JMenuItem("编辑RSS源");
		JMenu style = getTwoSecondMenu();
		// 添加事件处理
		// 为编辑RSS源添加事件处理
		edit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new EditRSS();
			}
		});

		// 添加子菜单
		menu.add(edit);
		menu.add(style);
		// 返回父菜单
		return menu;
	}
	// 获取第二个菜单的二级菜单
	/*
	 * 父级菜单：设置样式
	 * 子级菜单：Swing的plaf列表数据
	 */
	private JMenu getTwoSecondMenu()
	{
		// 创建父菜单
		JMenu menu = new JMenu("设置样式");
		// 设置子菜单

		// 返回父菜单
		return menu;
	}

	// 获取第三个菜单
	/*
	 * 菜单：帮助
	 */
	private JMenu getThreeMenu()
	{
		// 返回菜单
		return getThreeFirstMenu();
	}
	// 获取第三个菜单的一级菜单
	/*
	 * 父级菜单名：帮助
	 * 子级菜单名：关于RSS阅读器
	 */
	private JMenu getThreeFirstMenu()
	{
		// 新建父菜单
		JMenu menu = new JMenu("帮助");
		// 设置子菜单
		JMenuItem item = new JMenuItem("关于RSS阅读器...");
		// 添加事件处理
		item.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 设置状态栏
				ViewSetting.setStateBar("版本信息@CopyAllRight by newcih");
				JOptionPane.showMessageDialog(null, "版本v2.1\n作者new cih\n");
				// 状态栏回归
				ViewSetting.setStateBar("");
			}
		});
		// 添加子菜单
		menu.add(item);
		// 返回父菜单
		return menu;
	}	

	// 获取新闻显示区（用以替代新闻文本区）
	private JSplitPane getJPNewsDescription()
	{	
		// 建立分割面板的子组件
		JPanel jpNorth = new JPanel(new GridLayout(3, 1));

		// 声明并创建变量
		JLabel jlTitle = new JLabel("标题");
		jlTitle.setPreferredSize(new Dimension(jlWidth, jtHeight));
		JLabel jlLink = new JLabel("链接");
		jlLink.setPreferredSize(new Dimension(jlWidth, jtHeight));
		JLabel jlAuthor = new JLabel("作者");
		jlAuthor.setPreferredSize(new Dimension(jlWidth, jtHeight));
		JLabel jlPubDate = new JLabel("发布时间");
		jlPubDate.setPreferredSize(new Dimension(jlWidth, jtHeight));
		JButton jbLink = new JButton("查看详情");
		// 构建成员
		// 设置 标题 文本框
		jtTitle = new JTextField();
		jtTitle.setEditable(false);
		jtTitle.setPreferredSize(new Dimension(jtTitleWidth, jtHeight));
		jtLink = new JTextField();
		jtLink.setEditable(false);
		// 链接文本框的宽度减少用以添加链接按钮
		jtLink.setPreferredSize(new Dimension(jtLinkWidth, jtHeight));
		jtAuthor = new JTextField();
		jtAuthor.setEditable(false);
		jtAuthor.setPreferredSize(new Dimension(jtAuthorWidth, jtHeight));
		jtPubDate = new JTextField();
		jtPubDate.setEditable(false);
		jtPubDate.setPreferredSize(new Dimension(jtPubDateWidth, jtHeight));
		jtNews = new JTextArea();
		// 为链接按钮添加事件
		jbLink.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 如果链接存在，则允许调用并跳转
				if (jtLink.getText() != null)
					Browser.openURL(jtLink.getText());
				else
					JOptionPane.showMessageDialog(null, "无法链接到不存在的地址");
			}
		});
		jtNews.setEditable(false);
		// 将组件整理成3个JPanel
		JPanel jpTitle = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpTitle.add(jlTitle);
		jpTitle.add(jtTitle);
		JPanel jpLink = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpLink.add(jlLink);
		jpLink.add(jtLink);
		jpLink.add(jbLink);
		JPanel jpAuthorAndPubDate = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpAuthorAndPubDate.add(jlAuthor);
		jpAuthorAndPubDate.add(jtAuthor);
		jpAuthorAndPubDate.add(jlPubDate);
		jpAuthorAndPubDate.add(jtPubDate);
		// 添加组件到north面板
		jpNorth.add(jpTitle);
		jpNorth.add(jpLink);
		jpNorth.add(jpAuthorAndPubDate);
		// 添加组件到south面板
		jtNews.setLineWrap(true);
		JScrollPane jspNews = new JScrollPane(jtNews);
		jtNews.setPreferredSize(new Dimension(jtNewsWidth, jtNewsHeight));

		// 构建该分割面板并返回
		JSplitPane main = new JSplitPane(JSplitPane.VERTICAL_SPLIT, false, jpNorth, jspNews);
		main.setSize(new Dimension(WIDTH, jtNewsHeight));
		return main;	
	}
	// 创建带滚动条的表格
	private JScrollPane getJSPTable()
	{
		JScrollPane jspTable = null;

		if (jtTable == null)
		{
			// 创建表格数据模型，并且添加各列标题
			dtmTableModel = new DefaultTableModel();
			dtmTableModel.addColumn("主题");
			dtmTableModel.addColumn("接收时间");
			dtmTableModel.addColumn("发布时间");
			dtmTableModel.addColumn("作者");
			jtTable = new JTable(dtmTableModel);
			jspTable = new JScrollPane(jtTable);

			// 添加鼠标事件监听器
			jtTable.addMouseListener(new MouseAdapter()
			{
				public void mouseClicked(MouseEvent e)
				{
					// 判断鼠标是否单击
					if (e.getClickCount() == 1)
					{
						int selectedRow = jtTable.getSelectedRow();
						News selectedNews = newsList.get(selectedRow);

						if (selectedNews != null)
						{
							// 将新闻详细信息显示在组件集中
							jtTitle.setText(selectedNews.getTitle());
							jtLink.setText(selectedNews.getLink());
							jtAuthor.setText(selectedNews.getAuthor());
							jtPubDate.setText(selectedNews.getPubDate());
							jtNews.setText(selectedNews.getDescription());
							jtNews.setCaretPosition(0);
						}
					}
				}
			});
		}
		// 设置组件大小
		jspTable.setPreferredSize(new Dimension(tableWidth, tableHeight));
		return jspTable;
	}
	// 在表格中显示新闻信息
	public void showTable(List<News> newsList)
	{
		// 清空表格的内容(如果用removeRow发生ArrayIndexOutOfBoundException，请去http://blog.sina.com.cn/s/blog_b7c09bc00101dipa.html)
		dtmTableModel.getDataVector().clear();
		dtmTableModel.fireTableDataChanged();
		jtTable.updateUI();
		// 如果新闻内容列表不存在
		if (newsList == null)
			return;
		// 遍历新闻内容列表，将相应的新闻内容显示到列表中
		for (int i = 0; i < newsList.size(); i++)
		{
			News news = newsList.get(i);
			// 按指定的时间格式，获得当前日期
			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String currentDate = myFormat.format(new Date());
			// 将新闻的标题，当前日期，发布日期和作者显示在表格中
			String[] data = {news.getTitle(), currentDate, news.getPubDate(), news.getAuthor()};
			dtmTableModel.addRow(data);
		}
	}
	// 定义方法使界面显示在中心
	private void setCenter()
	{
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		this.setLocation((screenSize.width - WIDTH)/2, (screenSize.height - HEIGHT)/2);
	}

}

// 自定义进度条类
@SuppressWarnings("serial")
class MyProgressBar extends JFrame implements ActionListener, ChangeListener
{
	JProgressBar progressbar;
	JLabel label;
	Timer timer;
	private final int WIDTH = 500;
	private final int HEIGHT = 100;
	// 自定义显示
	String tip;
	// key值
	private int value;

	public MyProgressBar(String tip, int time)
	{
		// 初始化变量
		value = 1;
		// 设置界面
		setLayout(new BorderLayout(10, 10));
		label = new JLabel(tip);
		progressbar = new JProgressBar();
		progressbar.setOrientation(JProgressBar.HORIZONTAL);
		progressbar.setMinimum(0);
		progressbar.setMaximum(100);
		progressbar.setValue(0);
		progressbar.setStringPainted(true);
		progressbar.addChangeListener(this);
		progressbar.setPreferredSize(new Dimension(200, 30));
		// 设置关闭事件
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				setVisible(false);
				// 释放资源
				dispose();
			}
		});
		timer = new Timer(time, this);
		timer.start();

		this.tip = tip;

		add(label, BorderLayout.NORTH);
		add(progressbar, BorderLayout.SOUTH);
		setSize(WIDTH, HEIGHT);
		// 将进度条设置居中显示
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		setLocation((screenSize.width - WIDTH)/2, (screenSize.height - HEIGHT)/2);

		setVisible(true);
	}

	// 为计时器和进度条添加事件处理
	@Override
	public void actionPerformed(ActionEvent e)
	{
		value = Download.getValue();

		// 如果value==0,即为下载错误
		if (value == 0)
		{
			// 关闭进度条
			timer.stop();
			progressbar.setValue(0);
			setVisible(false);
			// 提示
			JOptionPane.showMessageDialog(null, "下载错误");
		}
		if (value < 100)
		{
			progressbar.setValue(value);
		}
		else
		{
			timer.stop();
			progressbar.setValue(0);
			setVisible(false);
		}
	}

	// 为进度条标签添加处理
	@Override
	public void stateChanged(ChangeEvent e)
	{
		int value = progressbar.getValue();

		label.setText(tip+"："+Integer.toString(value)+" %");
	}
}

// 自定义获取用户输入RSS界面类，用以创建新频道
@SuppressWarnings("serial")
class CreateChannelFrame extends JFrame  
{
	// 创建服务对象
	private RSSService rssService;
	private int WIDTH = 400;
	private int HEIGHT = 200;

	public CreateChannelFrame()
	{
		// 初始化变量
		rssService = new RSSService();
		// 界面参数设置
		setTitle("添加RSS频道");
		setSize(WIDTH, HEIGHT);
		setLayout(new GridLayout(3, 2, 10, 10));
		setResizable(false);
		// 放置在中心
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		this.setLocation((screenSize.width - WIDTH)/2, (screenSize.height - HEIGHT)/2);
		// 创建组件
		JLabel jlName = new JLabel("频道名称");
		JLabel jlUrl = new JLabel("URL路径");
		final JTextField jtName = new JTextField(30);
		final JTextField jtUrl = new JTextField(30);
		JButton mark = new JButton("订阅");
		JButton cannel = new JButton("取消");
		// 添加组件
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		p1.add(jlName);
		p1.add(jtName);
		p2.add(jlUrl);
		p2.add(jtUrl);
		p3.add(cannel);
		p3.add(mark);
		add(p1);
		add(p2);
		add(p3);
		// 设置窗口关闭事件
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				setVisible(false);
				// 释放资源
				dispose();
			}
		});
		// 订阅的事件处理
		mark.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 检测输入是否为空
				if (jtUrl.getText().equals("") || jtUrl.getText() == null
						|| jtName.getText().equals("") || jtName.getText()== null)
					return ;

				/// 检测用户输入的URL是否有效
				if(!MyToolkits.checkURL(jtUrl.getText().trim()))
				{
					JOptionPane.showMessageDialog(null, "输入的URL无效");
					return;
				}

				// 检测当前是否已经有该频道

				for (Channel channel : rssService.getChannelList())
				{					
					if (channel.getUrl().equals(jtUrl.getText().trim()))
					{
						JOptionPane.showMessageDialog(null, "您已经订阅过该频道");
						return;
					}
					else if (channel.getName().equals(jtName.getText().trim()))
					{
						JOptionPane.showMessageDialog(null, "您的频道列表里已经有该名字频道，请输入新名字");
						return;
					}
				}

				// 检测到用户输入正确且频道未被建立，则建立新频道
				Channel channel = new Channel(jtName.getText(), jtUrl.getText());
				rssService.addChannel(channel);
				// 订阅成功后应当下载该XML文件
				if (rssService.downloadXML(jtUrl.getText().trim()) != null)
				{

					JOptionPane.showMessageDialog(null, "订阅成功");
				}
				else
					JOptionPane.showMessageDialog(null, "订阅失败");
				
				// 执行关闭窗口操作
				setVisible(false);
				// 释放资源
				dispose();
			}
		});
		// 取消事件处理
		cannel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
				// 释放资源
				dispose();
			}
		});
	}
}

@SuppressWarnings("serial")
// 自定义编辑RSS的组件
class EditRSS extends JFrame
{
	// 组件变量
	@SuppressWarnings("rawtypes")
	JComboBox rssList;
	final JTextField jtTitle = new JTextField(30);
	final JTextField jtLink = new JTextField(30);

	// 大小设置
	final private int WIDTH = 400;
	final private int HEIGHT = 200;
	// 设置服务对象
	RSSService rssService;

	public EditRSS()
	{
		// 初始化变量
		rssService = new RSSService();
		// 设置参数
		setSize(WIDTH, HEIGHT);
		setLayout(new BorderLayout(10, 10));
		// 放置在中心
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		this.setLocation((screenSize.width - WIDTH)/2, (screenSize.height - HEIGHT)/2);

		// 添加组件
		add(getComboBox(), BorderLayout.NORTH);
		add(getEdit(), BorderLayout.CENTER);
		// 设置可视
		setVisible(true);
		// 设置窗口关闭事件
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				// 关闭窗口并释放资源
				setVisible(false);
				dispose();
			}
		});
	}
	// 获取带有频道列表的组件
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox getComboBox()
	{
		// 创建组件
		rssList = new JComboBox(rssService.getChannelList().toArray());
		// 设置组件
		// 设置事件
		rssList.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 将数据显示到文本框中
				jtTitle.setText(((Channel)rssList.getSelectedItem()).getName());
				jtLink.setText(((Channel)rssList.getSelectedItem()).getUrl());
			}
		});
		// 返回组件
		return rssList;
	}
	// 获取带有编辑功能的编辑区
	private JPanel getEdit()
	{
		// 创建父界面
		JPanel main = new JPanel(new GridLayout(3,1));
		// 标签组件
		JLabel jlTitle = new JLabel("标题");
		JLabel jlLink = new JLabel("URL");

		// 按钮组件
		JButton jbDelete = new JButton("删除该频道");
		JButton jbEdit = new JButton("确定修改");
		JButton jbCannel = new JButton("取消");
		// 添加组件
		JPanel p1 = new JPanel(new FlowLayout());
		p1.add(jlTitle);
		p1.add(jtTitle);
		JPanel p2 = new JPanel(new FlowLayout());
		p2.add(jlLink);
		p2.add(jtLink);
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		p3.add(jbCannel);
		p3.add(jbDelete);
		p3.add(jbEdit);
		// 设置事件处理
		// 设置取消按钮事件
		jbCannel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 关闭窗口并释放资源
				setVisible(false);
				dispose();
			}
		});
		// 设置删除按钮事件
		jbDelete.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 找到选中的频道并删除
				if (rssService.deleteChannel(((Channel)rssList.getSelectedItem()).getName()))
					JOptionPane.showMessageDialog(null, "频道删除成功");
			}
		});
		// 设置修改事件
		jbEdit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 检测数据
				// 检测标题
				for (Channel channel : rssService.getChannelList())
				{
					if (channel.getName().equals(jtTitle.getText()))
					{
						JOptionPane.showMessageDialog(null, "该标题已经存在");
						return;
					}
				}
				// 检测链接
				if (!MyToolkits.checkURL(jtLink.getText()))
				{
					JOptionPane.showMessageDialog(null, "该链接无效，请重新输入");
					return;
				}
				// 将数据更新到channelist中
				if (rssService.setChannel(((Channel)rssList.getSelectedItem()).getName(), jtTitle.getText(), jtLink.getText()))
					JOptionPane.showMessageDialog(null, "频道修改成功");
			}
		});
		// 添加组件
		main.add(p1);
		main.add(p2);
		main.add(p3);
		// 返回组件
		return main;
	}
}