/**
 * 
 */
package com.ruanko.model;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;

import com.ruanko.view.JMainFrame;

/**
 * @author new cih
 *
 */
public class MyToolkits {

	// 检测输入文件是否为xml文件
	@SuppressWarnings("resource")
	public static boolean checkXML(File file)
	{
		if (file == null)
			return false;
		if (!file.exists())
			return false;
		if (file.isDirectory())
			return false;
		
		FileInputStream fis = null;
		// 缓存字节数组,用以读取一部分文件数据来检测文件类型
		byte[] buffer = new byte[16];
		StringBuffer str = new StringBuffer();
		
		try{
			fis = new FileInputStream(file);
			fis.read(buffer);
			// 读取文件的头16个字节数据并获取其16进制表示字符串
			for (int i = 0; i < buffer.length; i++)
				str.append(Integer.toHexString((int)buffer[i]));
		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}finally{
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		// 如果对比正确,则为XML文件
		// StringBuffer没有重写equals方法,所以比较的是地址,这里先转为String再equals
		if (str.toString().equals("3c3f786d6c2076657273696f6e3d2231") || str.toString().equals("3f3c6d78206c657673726f693d6e3122"))                                                                      
		{			
			return true;
		}
		else
			return false;
	}
	// 检测输入的XML文件的URL是否有效
	public static boolean checkURL(String strUrl)
	{
		// 正则表达式匹配格式
		if (!strUrl.matches("^http://.+"))
			return false;
		// 网络检测是否可达
		HttpURLConnection httpURLConnection = null;
		URL url = null;
		
		try{
			url = new URL(strUrl);
			httpURLConnection = (HttpURLConnection)url.openConnection();
			// 设置连接网络的超时时间
			httpURLConnection.setConnectTimeout(4000);
			httpURLConnection.setDoInput(true);
			// 表示设置本次HTTP请求使用GET方式请求
			httpURLConnection.setRequestMethod("GET");
			int responseCode = httpURLConnection.getResponseCode();
			
			if (responseCode != 200)
				return false;
			// 尝试能否下载到该xml,所以这里的inputstream并未在以后用到,故注解为unuse
			@SuppressWarnings("unused")
			InputStream inputstream = httpURLConnection.getInputStream();

		}catch(java.net.SocketTimeoutException e1)
		{
			e1.printStackTrace();
			return false;
		}catch(MalformedURLException e2)
		{
			e2.printStackTrace();
			// 捕捉到错误则返回false
			return false;
		}catch(IOException e3)
		{
			e3.printStackTrace();
			// 捕捉到错误则返回false
			return false;
		}
		
		
		// 否则返回true
		return true;
	}
	// 测试方法  
	public static void main(String[] args)
	{
		// 测试getXMLFile方法
		List<File> filelist = getXMLFile(new File("NewsFiles"));

		for (File f : filelist)
			System.out.println(f.getName());
	}

	// 通过给定路径名获取当下所有XML格式文件
	public static List<File> getXMLFile(File filePath)
	{
		final List<File> fileList = new ArrayList<File>();


		if (filePath == null)
			return null;
		if (filePath.isFile())
		{
			if (filePath.getName().endsWith(".xml"))
				fileList.add(filePath);
		}
		/*
		@SuppressWarnings("unused")
		File[] temp = filePath.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File pathName)
			{
				if (pathName.isFile())
				{
					if (pathName.getName().endsWith(".xml"))
					{
						fileList.add(pathName);
						return true;
					}
				}
				// 递归搜索
				getXMLFile(pathName);	
				return true;
			}
		});
		*/
		return fileList;
	}
	// 通过获取表格的内容来自适应调整列宽
	public static JTable autoTableWidth(JTable jt, DefaultTableModel dtm)
	{
		// 实现动态设置表格列宽
		int contentLength = 0;			// 内容最大长度
		int accDateLength = 0;			// 接收日期长度
		int pubDateLength = 0;			// 发布日期长度
		int authorLength = 0;			// 作者名称长度
		int allLength = 0;				// 总长度

		// 先检测该XML文件中是否有数据
		if (dtm == null)
			return null;
		// 先检测该XML文件中是否有内容数据
		if (dtm.getValueAt(0, 0) != null)
		{
			// 获取内容最大长度
			for(int i = 0; i < dtm.getRowCount(); i++)
			{
				if (contentLength < ((String)dtm.getValueAt(i, 0)).trim().length())
					contentLength = ((String)dtm.getValueAt(i, 0)).length();
			}
		}

		// 先检测该XML文件中是否有作者数据
		if (dtm.getValueAt(0, 3) != null)
		{
			// 获取作者名称最大长度
			for(int i = 0; i < dtm.getRowCount(); i++)
			{
				if (authorLength < ((String)dtm.getValueAt(i, 3)).trim().length())
					authorLength = ((String)dtm.getValueAt(i, 3)).length();
			}
		}

		// 先检测是否有日期数据
		if (dtm.getValueAt(0, 1) != null)
		{
			// 获取接收日期长度和发布日期长度
			accDateLength = ((String)dtm.getValueAt(1, 1)).trim().length();
		}
		if (dtm.getValueAt(0, 2) != null)
		{
			pubDateLength = ((String)dtm.getValueAt(1, 2)).trim().length();
		}

		// 获取总长度
		allLength = contentLength+10+accDateLength+pubDateLength+authorLength;

		jt.getColumnModel().getColumn(0).setPreferredWidth((JMainFrame.WIDTH*(10+contentLength)/allLength));
		jt.getColumnModel().getColumn(1).setPreferredWidth(JMainFrame.WIDTH*accDateLength/allLength);
		jt.getColumnModel().getColumn(2).setPreferredWidth(JMainFrame.WIDTH*pubDateLength/allLength);
		jt.getColumnModel().getColumn(3).setPreferredWidth(JMainFrame.WIDTH*authorLength/allLength);

		return jt;
	}

	// 统一为组件设置字体
	public static void setUIFont(Font font)
	{
		FontUIResource fontRes = new FontUIResource(font);
		for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys.hasMoreElements();)
		{
			Object key = keys.nextElement();
			Object value = UIManager.get(key);

			if (value instanceof FontUIResource)
				UIManager.put(key, fontRes);
		}
	}
}
