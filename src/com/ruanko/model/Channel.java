package com.ruanko.model;

@SuppressWarnings("serial")
public class Channel implements java.io.Serializable {
	// 频道名称
	private String name;
	// 本地频道文件路径
	private String filePath;
	// 网络频道文件路径
	private String url;
	
	// 构造方法
	public Channel()
	{
		this.name = "";
		this.filePath = "";
		this.url = "";				
	}
	public Channel(String name, String url)
	{
		this.name = name;
		this.url = url;
		// 文件路径根据url的文件名构建
		this.filePath = new String("NewsFiles"+url.substring(url.lastIndexOf("/"), url.length()));
	}
	
	// 重写toString方法
	@Override
	public String toString()
	{
		return this.name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
