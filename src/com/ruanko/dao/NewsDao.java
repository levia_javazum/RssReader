/**
 * 
 */
package com.ruanko.dao;

import java.io.File;

import com.ruanko.model.News;

/**
 * @author new cih
 *
 */
public interface NewsDao {
	boolean save(News news, File file);
}

