/**
 * 
 */
package com.ruanko.service;

import com.ruanko.view.JMainFrame;

/**
 * @author newcih
 *
 */
public class ViewSetting {

	public static void setStateBar(String state)
	{
		JMainFrame.jlState.setText(state);
	}
}
