/**
 * 
 */
package com.ruanko.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.swing.JOptionPane;

import com.ruanko.model.Channel;
/**
 * @author new cih
 *
 */
public class UpdateThread extends Thread{
	private final static int TIMEOUT = 5*1000;
	private final static int DELAY_TIME = 60*1000;
	private final static int BUFFER_SIZE = 1255356;
	private final static RSSService rssService = new RSSService();
	

	// 线程运行方法
	@Override
	public void run()
	{
		while(true)
		{
			// 如果频道列表不为空
			if (rssService.getChannelList().size() > 0)
			{
				for (Channel channel : rssService.getChannelList())
				{
					//update(channel.getUrl(), channel.getFilePath());
					Download.download(channel.getUrl());
					System.out.println("正在更新"+channel.getName());
				}

				try{
					Thread.sleep(DELAY_TIME);
				}catch(InterruptedException e)
				{}
			}
		}
	}

	// 从网络地址下载RSS文件，保存到dest路径下
	public void update(String src, String dest)
	{
		// 创建网络连接
		HttpURLConnection httpConn = null;
		URL url = null;
		try {
			url = new URL(src);
			// 如果url创建成功
			if (url != null)
			{
				httpConn = (HttpURLConnection)url.openConnection();
				httpConn.setConnectTimeout(TIMEOUT);
				httpConn.setDoInput(true);
				httpConn.setRequestMethod("GET");
				
				httpConn.connect();
			}
		} catch(java.net.SocketTimeoutException e0)
		{
			e0.printStackTrace();
			return;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		// 判断是否有RSS文件更新
		File file = new File(dest);
		// 防止文件被中途删除
		if (!file.exists())
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 如果文件有新
		if (hasNewRSS(httpConn, new File(dest)))
		{
			saveAs(download(httpConn), file);
		}

	}

	// 下载文件到缓冲区
	private ByteBuffer download(HttpURLConnection httpConn)
	{
		InputStream in = null;
		ByteBuffer buffer = null;

		try {
			// 如果网络链接有问题
			if (httpConn == null)
				return null;
			
			in = httpConn.getInputStream();
			// 将数据写入缓冲区
			buffer = ByteBuffer.allocate(BUFFER_SIZE);

			if (in != null)
			{
				byte[] temp = new byte[1024];
				while((in.read(temp)) != -1)
				{
					buffer.put(temp);
				}
			}
			// 缓冲区写完
			// 反转
			buffer.flip();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 如果缓冲区中途发生问题，则应该取消本次下载
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 如果缓冲区中途发生问题，则应该取消本次下载
			return null;
		} finally{
			try{
				if (in != null)
				{
					in.close();
				}
				if (httpConn != null)
				{
					httpConn.disconnect();
				}
			}catch(Exception e)
			{}
		}
		
		// 返回已经写好的缓冲区
		return buffer;
	}

	// 将缓冲区中的文件保存到文件中
	private synchronized void saveAs(ByteBuffer buffer, File file)
	{
		FileOutputStream fileOutputStream = null;
		FileChannel channel = null;
		
		try {
			fileOutputStream = new FileOutputStream(file);
			channel = fileOutputStream.getChannel();
			channel.write(buffer);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try{
				if (channel != null)
				{
					channel.close();
				}
				if (buffer != null)
				{
					buffer.clear();
				}
			}catch(Exception e)
			{}
		}
	}

	// 判断RSS文件是否有更新
	private boolean hasNewRSS(HttpURLConnection httpConn, File file)
	{
		long current = System.currentTimeMillis();
		// 获取服务器最后修改时间
		long httpLastModified = httpConn.getHeaderFieldDate("Last-Modified", current);
		// 获取本地文件最后修改时间
		long fileLastModified = file.lastModified();
		
		// 如果服务器最后修改时间比本地最后修改时间大，则说明有更新
		if (httpLastModified > fileLastModified)
			return true;
		return false;
	}
	
	// 定义main方法，实现下载功能
	public static void main(String[] args)
	{

	}
}
